import { Component } from '@angular/core';
import { AlertService } from "./alert.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  value: string;
  title = 'Livro Angular 2';
  image: string = 'favicon.ico';
  dev: string [] = ['Html', 'Css', 'JavaScript', 'Angular'];

  constructor(private service: AlertService) { }

  sendMsg(): void {
    this.service.alertMsg();
  }

  valueSent(v) {
    this.value = v;
  }

}
