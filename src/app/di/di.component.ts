import { LogsService } from './logs.service';
import { NameTechnologiesService } from './name-technologies.service';
import { Component, OnInit } from '@angular/core';

@Component({
  providers:[NameTechnologiesService, LogsService],
  selector: 'app-di',
  templateUrl: './di.component.html',
  styleUrls: ['./di.component.css']
})
export class DiComponent implements OnInit {
  technologies: string[] = [];
  nameTecService: NameTechnologiesService;
  logsService: LogsService;
  constructor() { 
    this.logsService = new LogsService;
    this.nameTecService = new NameTechnologiesService(this.logsService);
    this.technologies = this.nameTecService.getNamesTechnologies();
  }

  ngOnInit() {
  }

}
