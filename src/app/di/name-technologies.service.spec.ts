import { TestBed, inject } from '@angular/core/testing';

import { NameTechnologiesService } from './name-technologies.service';

describe('NameTechnologiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NameTechnologiesService]
    });
  });

  it('should be created', inject([NameTechnologiesService], (service: NameTechnologiesService) => {
    expect(service).toBeTruthy();
  }));
});
