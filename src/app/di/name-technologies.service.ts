import { LogsService } from './logs.service';
import { Injectable } from '@angular/core';

@Injectable()
export class NameTechnologiesService {
  logsService: LogsService;

  constructor(logsService: LogsService) {
    this.logsService = logsService;
   }

  getNamesTechnologies(): string[] {
    this.logsService.setLog('consultou arra de Tecnologias');
    return ['Angula 4', 'TypeScript', 'Php', 'Java'];
  }
}
