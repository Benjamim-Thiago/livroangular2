import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBindingPasswordComponent } from './event-binding-password.component';

describe('EventBindingPasswordComponent', () => {
  let component: EventBindingPasswordComponent;
  let fixture: ComponentFixture<EventBindingPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventBindingPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBindingPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
