import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding-password',
  templateUrl: './event-binding-password.component.html',
  styleUrls: ['./event-binding-password.component.css']
})
export class EventBindingPasswordComponent implements OnInit {
  enabledButton: boolean = false; 
  values: string[] = [];
  age: number = 0;
  constructor() { }

  checkPass(value: string): void {
    if(value.length >= 5) {
      this.enabledButton = true;
    } else {
      this.enabledButton = false;
    }
  }

  viewAge(value): void {
    let year = new Date();
    this.age = year.getFullYear() - value;
  }

  add(value: string):void {
    this.values.push(value);
  }

  storagePass(value): void {
    alert('Senha ' + value + ' salva com sucesso!!!');
  }

  ngOnInit() {
  }

}
