import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {

  constructor() { }

  myClick():void {
    console.log('botão foi Clicado');
  }

  digito($event): void {
    console.log($event);
  }

  keyPressVarTemplate(value): void {
    console.log(value);
  }

  ngOnInit() {
  }

}
