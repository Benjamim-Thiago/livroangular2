import { Component, OnInit } from '@angular/core';
import { Contact } from "./contact";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  contact = new Contact('Ben', '(86)9-9544-2268', 'bebenjamimthiago@mail.com')
  constructor() { }

  sendData(): void {
    alert(`Seu nome é: ${this.contact.fullname} \n Seu Telefone é: ${this.contact.phone} \n Seu e-mail é: ${this.contact.email}`);
  }

  ngOnInit() {
  }

}
