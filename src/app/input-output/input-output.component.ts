import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input-output',
  templateUrl: './input-output.component.html',
  styleUrls: ['./input-output.component.css']
})
export class InputOutputComponent implements OnInit {
  @Input() menu: string;
  @Output() nameToClick = new EventEmitter();
  constructor() { }

  sendName(value) {
    this.nameToClick.emit(value);
  }
  ngOnInit() {

  }

  
}
