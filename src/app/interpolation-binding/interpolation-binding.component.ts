import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interpolation-binding',
  templateUrl: './interpolation-binding.component.html',
  styleUrls: ['./interpolation-binding.component.css']
})
export class InterpolationBindingComponent implements OnInit {

  constructor() { }
  
  getBook(): string {
    return 'Livro Angular 2';
  }
  getNumber(): number {
    return 6;
  }
  ngOnInit() {
  }

}
