import { Component, OnInit } from '@angular/core';
import { PessoaServiceService } from "./pessoa-service.service";

@Component({
  providers:[PessoaServiceService],
  selector: 'app-lista-pessoa',
  templateUrl: './lista-pessoa.component.html',
  styleUrls: ['./lista-pessoa.component.css']
})
export class ListaPessoaComponent implements OnInit {
  people: string[];
  name: string = 'Digite o nome';
  
  constructor(private pessoaService: PessoaServiceService) {
    this.people = pessoaService.getPessoas();
  }
  
  sendName() {
    this.pessoaService.setPessoa(this.name);
  }
  
  ngOnInit() {
  
  
  }
  
}
