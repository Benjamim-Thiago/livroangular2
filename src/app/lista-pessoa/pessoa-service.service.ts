import { Injectable } from '@angular/core';

@Injectable()
export class PessoaServiceService {

  constructor() { }

  namePerson: string [] = ['Raquel', 'James', 'Benjamim', 'Junior', 'Naryelle', 'Nadya'];

  getPessoas(): string[] {
    return this.namePerson;
  }

  setPessoa(name: string): void {
    this.namePerson.push(name);
  }

}
