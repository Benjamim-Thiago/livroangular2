import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-class',
  templateUrl: './ng-class.component.html',
  styleUrls: ['./ng-class.component.css']
})
export class NgClassComponent implements OnInit {
  valueClassBinding: boolean = false;
  
  constructor() { }

  toChangeClassBinding(): void {
    this.valueClassBinding = !this.valueClassBinding;
  }

  classes():any {
    let values = {
      'back-color': this.valueClassBinding,
      'color-text': this.valueClassBinding,
      'style-text': this.valueClassBinding,
      'paragraph-border': this.valueClassBinding
    }
    return values;
  }

  ngOnInit() {
  }

}
