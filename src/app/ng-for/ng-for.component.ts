import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.css']
})
export class NgForComponent implements OnInit {
  names: any[] = [
   {id: 1, name: 'João'} ,
   {id: 2, name: 'Maria'} ,
   {id: 3, name: 'Thiago'} ,
   {id: 4, name: 'José'} 
  ];
  constructor() { }

  mySave(index: number, names: any) {
    return names.id;
  }
  
  update() {
    this.names = [
      {id: 1, name: 'João'} ,
      {id: 2, name: 'Maria'} ,
      {id: 3, name: 'Thiago'} ,
      {id: 4, name: 'José'}, 
      {id: 5, name: 'Ben'} 
    ];
  }

  ngOnInit() {
  
  }

}
