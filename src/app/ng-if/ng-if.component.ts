import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-if',
  templateUrl: './ng-if.component.html',
  styleUrls: ['./ng-if.component.css']
})
export class NgIfComponent implements OnInit {
  count: number;
  nName: boolean;
  course: string[] = [];
  
  constructor() { }

  toast(): void {
    this.nName = !this.nName;
  }

  getValue(): boolean {
    return this.nName;
  }

  addCourse():void {
    this.course.push('Angular 4');
  }

  conterCourse(): number {
    let n = this.course.length;
    return n;
  }

  ngOnInit() {
  }

}
