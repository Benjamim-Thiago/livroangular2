import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  templateUrl: './ng-style.component.html',
  styleUrls: ['./ng-style.component.css']
})
export class NgStyleComponent implements OnInit {
  validColor: boolean = false;
  validFont: boolean = false;
  size: number = 20
  fontSize: string = this.size + 'px'; 
  constructor() { }

  increment(): void {
    this.size++;
    this.fontSize = this.size + 'px';
  }

  toChangeFont(): void {
    this.validFont = ! this.validFont
  }

  toChangeColor(): void {
    this.validColor = ! this.validColor;
  }
  
  ngOnInit() {
  }

}
