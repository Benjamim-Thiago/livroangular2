import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-binding',
  templateUrl: './property-binding.component.html',
  styleUrls: ['./property-binding.component.css']
})
export class PropertyBindingComponent implements OnInit {
  viewParagrath: boolean =true;
  constructor() { }

  toast(): boolean {
    return this.viewParagrath =! this.viewParagrath;
  }

  ngOnInit() {
  }

}
